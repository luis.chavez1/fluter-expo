import 'dart:convert';

import 'package:expo2/models/character.dart';
import 'package:http/http.dart' as http;

class RickAndMortyService {

  Future<List<Character>>getallCharacters() async {
    List<Character> characterList = [];
    try {
      var result = await http.get("https://rickandmortyapi.com/api/character/");
      Map<String,dynamic> jsonList = json.decode(result.body);
      List<dynamic> characterJsonList = jsonList['results'];
      characterJsonList.forEach((item) => characterList.add(Character.fromJson(item)));
      return characterList;
    } catch (error) {
      throw error;
    }
      
  }

}