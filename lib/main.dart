import 'package:flutter/material.dart';
import './service/rick_and_morty_service.dart';

//import 'package:expo2/erik.dart';

void main() => runApp(MyApp());



class MyApp extends StatelessWidget {
  @override
  Widget build(context) {
    return MaterialApp(
      home: Scaffold(
        body:  MyHomePage(title: "Rick and Morty"),
    ));
  }

  

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  RickAndMortyService service = RickAndMortyService();

  @override
  void initState() {
    super.initState();
    service.getallCharacters();


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(

        child: Text("Rick"),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
